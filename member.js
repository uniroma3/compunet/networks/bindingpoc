const {publicToAddress} = require("ethereumjs-util");

const {randomHex, soliditySha3} = require("web3-utils");

const {toBuffer, privateToPublic} = require("ethereumjs-util");

const { probabilisticSelection } = require("./binding-Utils");
const dgram = require('dgram');


module.exports = function (web3) {


    const sk = randomHex(32);
    const pk = privateToPublic(toBuffer(sk));


    this.sendChallenge = async function (newbindingEvent) {
        //console.log("event-Binding");
        //console.log(newbindingEvent);
        const value = newbindingEvent.returnValues
        const bHash = newbindingEvent.blockHash

        if(probabilisticSelection(value.R.toString('hex'), bHash, pk.toString('hex'))) {
            const endpoint = value.endpoint;

            //console.log(`pk = ${pk.toString('hex')} \t account = ${publicToAddress(pk).toString('hex')}`);
            const [ip, port] = endpoint.split(':');
            //console.log(`ip = ${ip}, port=${port}`)
            const Q_c = randomHex(5);
            //console.log(Q_c);

            const hash = soliditySha3(Q_c, value.R.toString('hex'));
            //console.log(hash);

            const signature = web3.eth.accounts.sign(hash, sk);
            //console.log(signature);

            const Pi_c = Q_c + '|' + JSON.stringify(signature);

            const socket = dgram.createSocket('udp4');

            //console.log('I send ' + Pi_c);
            socket.send(Pi_c, Number(port), ip, (r) => {
                //console.log('disconnect');
                socket.close();
            });
        }

    }



}