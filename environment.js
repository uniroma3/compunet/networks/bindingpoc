module.exports = {
    UrlProviderHTTPS: 'https://ropsten.infura.io/v3/cb841a5c15f44c189ccf9d1d749ee1ec',
    UrlProviderWSS: 'wss://ropsten.infura.io/ws/v3/cb841a5c15f44c189ccf9d1d749ee1ec',
    User:{
        sK: '0x18df641f6df346827d929eb24dbfb6e92777c1fccd27116dc743e074b4be179a',
        pK: '1d47d4d2f8493906b48fc85b9e0277195a9654f30db574b2d86334e3803fc594df91480f5d4f56e860bc8d05a6c28b6dccc01a7386acc0dd0d7c8c78cd2f9f01'
    }
}