const Web3 = require('web3');

const env = require("./environment.js");
const {toBuffer,bufferToInt} = require("ethereumjs-util");
const {k, verifyCommitteeSignature} = require("./binding-Utils");
const {asciiToHex, randomHex} = require("web3-utils");

const DidRegistryContract = require('ethr-did-registry');
const BindingRegistryContract = require("./Binding.json");


const dgram = require('dgram');

const { Resolver } = require('did-resolver');
const { getResolver } = require('ethr-did-resolver');

const { GetProof } = require('eth-proof');


module.exports = function () {


    const web3 = new Web3(new Web3.providers.HttpProvider(env.UrlProviderHTTPS));
    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.UrlProviderWSS));

    const sk = env.User.sK;
    const account = web3.eth.accounts.privateKeyToAccount(sk); //0xDa7376fF18B05959d91D3a812814576521D41F44


    const wallet = web3ws.eth.accounts.wallet.add(account);
    const keystore = wallet.encrypt(randomHex(32));

    let endPoint;
    let socket;

    let BindingRegistry;

    let requestHash;
    let requestBlockHash;
    let certificateTxHash;

    let VerEndPoint;

    let G = new Map();
    let C = [];


    this.newBindingRequest_endPoint_turnOn =  function () {

        socket = dgram.createSocket('udp4');

        socket.on('message', (msg, rinfo) => {
            //console.log(`user got: ${msg.toString()} from ${rinfo.address}:${rinfo.port}`);
            if(G.size < k){
                //console.log(`user got from ${rinfo.address}:${rinfo.port}`);
                const [Q_c, signatureObjectString] = msg.toString().split('|');
                const signatureJSON = JSON.parse(signatureObjectString);
                const committeeAccount = web3.eth.accounts.recover(signatureJSON);

                if(!C.includes(committeeAccount) && verifyCommitteeSignature(Q_c, signatureJSON.signature, requestHash, requestBlockHash)){
                    G.set(signatureJSON.signature, Q_c);
                    C.push(committeeAccount);
                    if(G.size === k){
                        //console.log(requestBlockHash);
                        const pay = BindingRegistry.methods.payCommittee(toBuffer(requestBlockHash), toBuffer(requestHash),Array.from(G.values()),Array.from(G.keys()));
                        let gas = 41598.1 *k + 30162.4
                    
                        pay.send({from: account.address,value:0 , gas: Math.trunc(gas*(1.5))})
                            .once('transactionHash', txhash => {
                                console.log(`Mining Certificate transaction ...`);
                                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
                                certificateTxHash = txhash;
                            }).then((receipt)=>{
                            //console.log(receipt);
                            updateDID();
                        });
                    }
                }
            }


        });

        socket.on('listening', () => {
            const address = socket.address(); //{"address":"127.0.0.1","family":"IPv4","port":41234}
            endPoint = `${address.address}:${address.port}`
            console.log(`server listening ${endPoint}`);
            newBindingRequest();
        });

        socket.bind(41234, 'localhost');

    }


    function newBindingRequest(){

        const bindingPair = `${env.User.pK}, ${endPoint}`
        console.log(bindingPair);

        BindingRegistry = new web3ws.eth.Contract(BindingRegistryContract.abi,BindingRegistryContract.BindingRegistryAddress);
        const newReq =  BindingRegistry.methods.newRequest(endPoint, env.User.pK);
        newReq.estimateGas({from: account.address, value: 1000000000000000}).then((gas)=> {
            newReq.send({from: account.address,gas: Math.trunc(gas*(1.5)), value: 1000000000000000}).
            once('transactionHash', txhash => {
                console.log(`Mining newRequest transaction ...`);
                console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
            }).then((r) => {
                //console.log("new Request receipt");
                //console.log(r);
                console.log("R: " + r.events.NewRequest.returnValues.R);
                requestHash = r.events.NewRequest.returnValues.R;
                requestBlockHash = r.events.NewRequest.blockHash;
            });
        });





    }

    this.setVerifierEndpoint = function (verifier){
        VerEndPoint = verifier.getEndPoint();
    }



    async function updateDID () {

        const DidReg = new web3ws.eth.Contract(DidRegistryContract.abi,DidRegistryContract.networks[3].address);

        const b32 = asciiToHex("did/svc/certifiedUDPEndpoint");


        const getProof = new GetProof(env.UrlProviderHTTPS);

        const { header, txProof, txIndex} = await getProof.transactionProof(certificateTxHash);
        const receiptAndMerkleProof = await getProof.receiptProof(certificateTxHash);


        const s = `{ "endpoint": "${endPoint}", "proof": { "tBlockNumber": "${bufferToInt(header.number)}", "tAndMerkleProof": "${txProof.toHex()}", "receiptAndMerkleProof": "${receiptAndMerkleProof.receiptProof.toHex()}", "tIndex": "${txIndex}"}}`

        const bvalue = toBuffer(asciiToHex(s));

        console.log("setAttribute");
        const setAttribute = DidReg.methods.setAttribute(account.address, b32, bvalue, 3000);
        setAttribute.estimateGas({from: account.address}).then(gas=>{
            setAttribute.send({from: account.address, gas: Math.trunc(gas*(2.5))})
                .then(()=>{
                    sendDID()
                    web3ws.currentProvider.disconnect();
                });
        });

    }

    async function sendDID() {

        console.log("start Resolve")
        const providerConfig = { rpcUrl: 'https://ropsten.infura.io/ethr-did' };
        const etherDidResolver = getResolver(providerConfig)

        const didResolver = new Resolver(etherDidResolver)

        const didDocument = await didResolver.resolve(`did:ethr:${account.address}`);
        //console.log(didDocument);

        socket.send(JSON.stringify(didDocument), Number(VerEndPoint.port), VerEndPoint.ip, (r) => {
            //console.log('disconnect');
            socket.close();
        });

    }


}