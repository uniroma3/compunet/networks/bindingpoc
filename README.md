# BindingPoC

This is the Proof of Concept (PoC) of the article in [[1](#1)].

We realized this PoC with the purpose of showing that the proposed approach can be implemented with state-of-the-art technologies and with the intent to estimate blockchain costs.

Our PoC realizes the _prover-receives_ method with a _probabilistic selection_ of the committee.


## Prerequisites

The only prerequisites for the PoC is NodeJs locally installed.

## Quickstart

We recommend to perform the following steps:

1. `git clone https://gitlab.com/uniroma3/compunet/networks/bindingpoc.git` 
2. `cd bindingpoc`
3. `npm install`
4. `node index.js`

## Specifications

* Endpoints are **<ip address, port>** pairs for UDP sockets. The prover's endpoint is `127.0.0.1:41234`. The verifier's endpoint is `127.0.0.1:41235`.
* The PoC is based on the **Ethereum** blockchain technology.
* It realizes $` N=1000 `$ potential committee members, one prover, and one verifier. 
* Other parameters for the probabilistic committee selection are $` k=20 `$ and $` \alpha = 3 `$.
* This PoC already provides an environment file (`environment.js`) containing the prover's pair of cryptographic keys and a link to connect to an Ethereum full-node through _Infura_ service. However, we suggest replacing this information with a customized one to create the tester's environment. 

### How to customize `environment.js` file (_optional_)

To create a customized environment, perform the following steps:

1. Create an account on [Infura](https://infura.io/). 
2. In the dashboard, create a new project.
3. Once created, in the `Settings` tab of the project, copy and paste your `https` and `wss` endpoints of the Ropsten network to `environment.js`
4. Create your prover's keys by executing the following code:

``` javascript
const {randomHex} = require("web3-utils");
const {toBuffer, privateToPublic} = require("ethereumjs-util");

const sk = randomHex(32);
const pk = privateToPublic(toBuffer(sk));     
```
5. Save the keys in `environment.js`
6. Fill the account (associated with the secret key `const account = web3.eth.accounts.privateKeyToAccount(sk);`) by using this [faucet](https://faucet.ropsten.be/). Note that, there are other faucets available.




# Bibliography

<a name="1">[1]</a> Diego Pennino, Maurizio Pizzonia, Andrea Vitaletti, Marco Zecchini. [Efficient Certification of Endpoint Control on Blockchain]()
