const Web3 = require("web3");

const env = require("./environment.js");

const {bytesToHex} = require("web3-utils");

const BindingRegistryContract = require("./Binding.json");


const { verifyCommitteeSignature, k} = require("./binding-Utils");

const abiDecoder = require('abi-decoder');
const {toChecksumAddress, toBuffer} = require("ethereumjs-util");

const dgram = require('dgram');

const {VerifyProof} = require('eth-proof');
const { Proof } = require('eth-object');

module.exports = function () {

    let endPoint;

    const web3 = new Web3(new Web3.providers.HttpProvider(env.UrlProviderHTTPS));



    const socket = dgram.createSocket('udp4');

    socket.on('message', (msg, rinfo) => {
        const DID_document = JSON.parse(msg.toString());
        console.log(DID_document);
        verifyDID(DID_document);
        socket.close()

    });

    socket.on('listening', () => {
        const address = socket.address(); //{"address":"127.0.0.1","family":"IPv4","port":41235}
        endPoint = {ip: address.address, port: address.port};
        console.log(`Verifier listening ${JSON.stringify(endPoint)}`);
    });

    socket.bind(41235, 'localhost');


    this.getEndPoint = function () {
        return endPoint;
    }


    async function verifyDID (DIDdocument) {
        for (const s of DIDdocument.service) {
            if (s.type === 'certifiedUDPEndpoint') {
                const bindingService = JSON.parse(s.serviceEndpoint);
                const proof = bindingService.proof;
                const blockNumber = proof.tBlockNumber;
                const tAndMerkleProof = Proof.fromHex(proof.tAndMerkleProof);
                const receiptAndMerkleProof = Proof.fromHex(proof.receiptAndMerkleProof);
                const tIndex = proof.tIndex;


                const receipt = await VerifyProof.getReceiptFromReceiptProofAt(receiptAndMerkleProof, tIndex);

                if (bytesToHex(receipt.postTransactionState) !== "0x01") throw new Error('Transaction rejected');

                const block = await web3.eth.getBlock(blockNumber); //It can be only the header.
                if (!block) throw new Error('BlockHeader not in chain');

                const receiptRootHeader = block.receiptsRoot;
                const receiptRootProof = VerifyProof.getRootFromProof(receiptAndMerkleProof);


                if (!receiptRootProof.equals(toBuffer(receiptRootHeader))) throw new Error('Receipt proof mismatch');

                const txRootHeader = block.transactionsRoot;
                const txRootProof = VerifyProof.getRootFromProof(tAndMerkleProof);


                if (!txRootProof.equals(toBuffer(txRootHeader))) throw new Error('Tx proof mismatch');

                const certificateTx = await VerifyProof.getTxFromTxProofAt(tAndMerkleProof, tIndex);

                if (toChecksumAddress("0x"+certificateTx.to.toString('hex')) !== BindingRegistryContract.BindingRegistryAddress) throw new Error('TX Error');
                abiDecoder.addABI(BindingRegistryContract.abi);

                const decodedInput = abiDecoder.decodeMethod("0x"+certificateTx.data.toString('hex'));

                if (decodedInput.name !== "payCommittee") throw new Error('Wrong method Error');

                const blockHash = decodedInput.params[0].value;
                const R = decodedInput.params[1].value;
                const Q_c_Array = decodedInput.params[2].value;
                const Signature_Array = decodedInput.params[3].value;

                try {
                    const C = [];
                    if (Q_c_Array.length < k) throw new Error('Partial Challenges size error');

                    for (let i = 0; i < k; i++) {
                        const pk = verifyCommitteeSignature(Q_c_Array[i], Signature_Array[i], R, blockHash);
                        if (C.includes(pk)) throw new Error('Partial Challenge signature Error');
                        C.includes(pk);
                    }
                    console.log('Valid Certificate');
                    return true;
                } catch (e) {
                    console.log(e);
                    console.log('No Valid Certificate');
                    return false;
                }

            }
        }

    }
}