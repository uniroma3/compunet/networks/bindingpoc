const Web3 = require("web3");

const env = require("./environment.js");



const { N } = require("./binding-Utils");

const cm = require("./member");

const BindingRegistryContract = require("./Binding.json");

module.exports = function () {

    console.log("Start Committee");
    let committeePool = [];

    const web3ws = new Web3(new Web3.providers.WebsocketProvider(env.UrlProviderWSS));
    const web3 = new Web3(new Web3.providers.HttpProvider(env.UrlProviderHTTPS));

    for (let i = 0; i < N; i++) {
        committeePool.push(new cm(web3));
    }

    const BindingReg = new web3ws.eth.Contract(BindingRegistryContract.abi,BindingRegistryContract.BindingRegistryAddress);

    BindingReg.events.NewRequest()
        .on('data', bindingEvent => {
            console.log("new request");
            setTimeout(()=>{
                committeePool.forEach(c=>c.sendChallenge(bindingEvent));
                web3ws.currentProvider.disconnect();
            }, 1500);
        });

}